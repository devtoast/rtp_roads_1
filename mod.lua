function data()
return {
  info = {
    minorVersion = 0,
    severityAdd = "NONE",
    severityRemove = "WARNING",
    name = _("Name"),
    description = _("Description"),
	tags = { "Street",},
	visible = true,
    authors = {
      {
		name = "easybronko",
		role = "Model & Script",
      },
      {
		name = "wicked1133",
		role = "UI & Script",
      },
      {
		name = "Yoshi",
		role = "Textures",
      },
      {
		name = "DomTrain",
		role = "Streetlights",
      },
    },
  },
}
end