function data()
return {
	name = _("RTP - Strassenschotter"),
	
	detailColorTexture = "terrain/rtp_texture_strassenschotter_albedo.dds",
	detailMetalGlossAoHTexture = "terrain/gravel_03_metal_gloss_ao_h.dds",
	detailNormalTexture = "terrain/gravel_03_nrml.dds",
	overlayTexture = "terrain/overlay_0.dds",

	detailSize = 1 / 16,
	overlaySize = 0.1,
	overlayStrength = 0.4,

	order = -20,
	priority = 1000000
}
end
