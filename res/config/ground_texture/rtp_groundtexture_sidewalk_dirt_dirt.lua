local tu = require "texutil"

function data()
return {
	texture = tu.makeMaterialIndexTexture("res/textures/terrain/material/country_sidewalk.tga", "REPEAT", "REPEAT"),
	texSize = { 64.0, 1.0 },
	materialIndexMap = {
		[160] = "dirt.lua",
		[255] = "dirt.lua",
	},
	
	priority = 6
}
end
