local tu = require "texutil"

function data()
return {
	texture = tu.makeMaterialIndexTexture("res/textures/terrain/material/country_sidewalk.tga", "REPEAT", "REPEAT"),
	texSize = { 32.0, 3 },
	materialIndexMap = {
		[160] = "shared/grass_cutted_01.lua",
		[255] = "shared/gravel_03.lua",
	},
	
	priority = 6
}
end
