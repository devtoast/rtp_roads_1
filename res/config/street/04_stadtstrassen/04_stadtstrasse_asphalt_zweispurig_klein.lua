function data()
return {
	numLanes = 2,
	streetWidth = 6.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = false,
	speed = 50.0,
	type = "country new small",
	name = _("Urban road asphalt small").." (22)",
	desc = _("Road, 2-lane, speed limit %2%."),
	categories = { "04_stadtstrassen" },
	sidewalkFillGroundTex = "rtp_groundtexture_sidewalk_grasscutted01_gravel03.lua",
	materials = {
		streetPaving = {
			name = "street/country_new_medium_paving.mtl",
			size = { 8.0, 6.0 }
		},
		streetBorder = {
		--	name = "street/country_new_large_border.mtl",
		--	size = { 24, 0.459 }	
		},	
		junctionBorder = {	
		--	name = "street/country_new_large_border.mtl",
		--	size = { 24, 0.459 }
		},			
		streetLane = {
			name = "street/new_medium_lane.mtl",
			size = { 2.5, 2.5 }
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		crossingLane = {
			name = "street/new_medium_lane.mtl",
			size = { 2.5, 2.5 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},	
	},
	assets = {
		{
			name = "asset/domtrain_kofferleuchte.mdl",
			offset = 8.0,
			distance = 16.0,
			prob = 1.0,
			offsetOrth = 0.3,
			randRot = false,
			oneSideOnly = true,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	cost = 25.0,
}
end
