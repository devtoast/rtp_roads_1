function data()
return {
	numLanes = 2,
	streetWidth = 6.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = false,
	speed = 30.0,
	type = "country old small",
	name = _("Urban road transparent").." (22)",
	desc = ( _("Road, 2-lane, speed limit %2%.") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture.")),
	categories = { "04_stadtstrassen" },
	materials = {
		streetPaving = {
			name = "street/rtp_paving_transparent.mtl",
			size = { 8.0, 0.05 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},		
	},
	cost = 25.0,
}
end
