function data()
return {
	numLanes = 4,
	streetWidth = 16.0,
	sidewalkWidth = 1.0,
	sidewalkHeight = .02,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = false,
	speed = 100.0,
	type = "country new large",
	name = _("Urban road asphalt").." (24)",
	desc = _("Road, 4-lane, speed limit %2%."),
	categories = { "04_stadtstrassen" },
	materials = {
		streetPaving = {
			name = "street/country_new_medium_paving.mtl",
			size = { 8.0, 8.0 }
		},		
		streetBorder = {
			name = "street/country_new_large_border.mtl",
			size = { 24, 0.459 }		
		},
		junctionBorder = {	
			name = "street/country_new_large_border.mtl",
			size = { 24, 0.459 }
		},			
		streetLane = {
			name = "street/new_medium_lane.mtl",
			size = { 3.0, 3.0 }
		},
		streetArrow = {
			name = "street/default_arrows.mtl",
			size = { 9.0, 3.0 }
		},
		streetStripe = {
			name = "street/country_new_medium_stripes.mtl",
			size = { 32.0, .5 }		
		},
		streetStripeMedian = {
			name = "street/country_new_large_median.mtl",
			size = { 4.0, .5 }		
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		streetBus = {
			name = "street/new_medium_bus.mtl",
			size = { 12, 2.7 }
		},
		crossingLane = {
			name = "street/new_medium_lane.mtl",
			size = { 3.0, 3.0 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},	
		sidewalkBorderInner = {
		name = "street/country_new_large_sidewalk_border_inner.mtl",	
			size = { 9, 1 }
		},
	},
	assets = {
		{
			name = "asset/domtrain_kofferleuchte.mdl",
			offset = 8.0,
			distance = 16.0,
			prob = 1.0,
			offsetOrth = 0.3,
			randRot = false,
			oneSideOnly = false,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	borderGroundTex = "rtp_groundtexture_streetborder_gravel03_gravel03.lua",
	sidewalkFillGroundTex = "rtp_groundtexture_sidewalk_grasscutted01_gravel03.lua",
	cost = 75.0,
}
end
