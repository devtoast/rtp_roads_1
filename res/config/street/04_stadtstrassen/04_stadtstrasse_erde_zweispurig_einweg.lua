function data()
return {
	laneConfig = {
		{ forward = true },
		{ forward = true },
		{ forward = true },
		{ forward = true },
	},
	streetWidth = 6.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .00,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = false,
	speed = 30.0,
	transportModesStreet = { "TRUCK" },
	type = "one way country new medium",
	name = _("Urban road dirt soil").." (12)",
	desc = ( _("Oneway road, 2-lane, speed limit %2%.") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture.")),
	categories = { "04_stadtstrassen" },
	materials = {
		streetLane = {
			name = "street/rtp_streetlane_erde.mtl",
			size = { 8.0, 3.0 }
		},
		sidewalkLane = {
			name = "street/rtp_streetlane_erde.mtl",
			size = { 8.0, 1.5 }
		},
		crossingLane = {
			name = "street/rtp_streetlane_erde.mtl",
			size = { 8.0, 3.0 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},	
	},
	cost = 50.0,
}
end
