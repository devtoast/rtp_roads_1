function data()
return {
	numLanes = 2,
	streetWidth = 6.0,
	sidewalkWidth = 0.3,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 50.0,
	type = "country new small",
	name = _("Country road asphalt small").." (22)",
	desc = _("Road, 2-lane, speed limit %2%."),
	categories = { "03_Landstrassen" },
	materials = {
		streetPaving = {
			name = "street/country_new_medium_paving.mtl",
			size = { 8.0, 8.0 }
		},				
		streetLane = {
			name = "street/new_medium_lane.mtl",
			size = { 2.5, 2.5 }
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		crossingLane = {
			name = "street/new_medium_lane.mtl",
			size = { 2.5, 2.5 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},	
	},	
	assets = {
		{
		name = "asset/leitpfosten_brd_schmal.mdl",
			offset = 0,
			distance = 25.0,
			prob = 1.0,
			offsetOrth = -0.2,
			randRot = false,
			oneSideOnly = false,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = false,
		}, 
	},
	sidewalkFillGroundTex = "rtp_groundtexture_sidewalk_grasscutted01_gravel03.lua",
	cost = 25.0,
}
end
