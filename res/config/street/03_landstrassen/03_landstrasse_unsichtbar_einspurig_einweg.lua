function data()
return {
	laneConfig = {
		{ forward = true },
		{ forward = true },
		{ forward = true },
	},
	streetWidth = 3.0,
	sidewalkWidth = 0.1,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 30.0,
	type = "one way country new small",
	name = _("Country road transparent").." (11)",
	desc = ( _("Oneway road, 1-lane, speed limit %2%.") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture.")),
	categories = { "03_Landstrassen" },
	materials = {
		streetPaving = {
			name = "street/rtp_paving_transparent.mtl",
			size = { 8.0, 0.05 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},		
	},
	cost = 25.0,
}
end
