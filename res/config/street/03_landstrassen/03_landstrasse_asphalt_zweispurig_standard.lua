function data()
return {
	numLanes = 2,
	streetWidth = 8.0,
	sidewalkWidth = 0.5,
	sidewalkHeight = .00,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	type = "country new medium",
	name = _("Country road asphalt").." standard (22)",
	desc = _("Road, 2-lane, speed limit %2%."),
	categories = { "03_Landstrassen" },
	materials = {
		streetPaving = {
			name = "street/country_new_medium_paving.mtl",			
		},		
		streetBorder = {
			name = "street/country_new_large_border.mtl",
			size = { 24, 0.459 }	
		},	
		junctionBorder = {	
			name = "street/country_new_large_border.mtl",
			size = { 24, 0.459 }
		},			
		streetLane = {
			name = "street/new_medium_lane.mtl",
			size = { 3.0, 3.0 }
		},
		streetStripeMedian = {
			name = "street/country_new_medium_stripes.mtl",
			size = { 32.0, .5 }		
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		crossingLane = {
			name = "street/new_medium_lane.mtl",
			size = { 3.0, 3.0 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},	
	},
	assets = {
		{
		name = "asset/leitpfosten_modern.mdl",
			offset = 0,
			distance = 25.0,
			prob = 1.0,
			offsetOrth = 0,
			randRot = false,
			oneSideOnly = false,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = false,
		}, 
	},
	sidewalkFillGroundTex = "rtp_groundtexture_sidewalk_grasscutted01_gravel03.lua",
	cost = 50.0,
}
end
