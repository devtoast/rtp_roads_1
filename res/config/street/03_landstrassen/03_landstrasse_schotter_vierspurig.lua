function data()
return {
	numLanes = 4,
	streetWidth = 12.0,
	sidewalkWidth = 0.1,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 30.0,

	type = "country old small",
	name = _("Country road ballast").." (24)",
	desc = ( _("Road, 4-lane, speed limit %2%.") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture.")),
	categories = { "03_Landstrassen" },
	materials = {			
		streetLane = {
			name = "street/rtp_streetlane_schotter.mtl",
			size = { 8.0, 3.0 }
		},
		crossingLane = {
			name = "street/rtp_streetlane_schotter.mtl",
			size = { 8.0, 3.0 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},	
	},
	assets = {
		{
		name = "asset/leitpfosten_brd_breit.mdl",
			offset = 0,
			distance = 25.0,
			prob = 1.0,
			offsetOrth = 0.1,
			randRot = false,
			oneSideOnly = false,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = false,
		}, 
	},
	cost = 25.0,
}
end
