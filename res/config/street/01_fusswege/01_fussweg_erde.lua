function data()
return {
	numLanes = 2,
	streetWidth = 0.1,
	sidewalkWidth = 1.5,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 5.0,
	transportModesStreet = { "BUS","TRUCK" },	
	type = "country old small",
	name = _("Footpath dirt soil").." (20)",
	desc = ( _("Road, 2-lane, speed limit %2%.") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture.")),
	categories = { _("01_fusswege")},
	materials = {	
		streetLane = {
			name = "street/rtp_streetlane_erde.mtl",
			size = { 2.0, 2.5 }
		},
		crossingLane = {
			name = "street/rtp_streetlane_erde.mtl",
			size = { 2.0, 2.5 }
		},
		crossingTram = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},	
		sidewalkBorderInner = {
			name = "street/rtp_streetlane_erde.mtl",
			size = { 2.0, 2.0 }
		},
	},
	cost = 25.0,
}
end
