function data()
return {
	laneConfig = {
		{ forward = true },
		{ forward = true },
		{ forward = true },
		{ forward = true },
		{ forward = true },
	},
	streetWidth = 12.0,
	sidewalkWidth = 1.0,
	sidewalkHeight = .3,
	yearFrom = 1850,
	yearTo = 0,	
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS" },
	type = "one way new large",
	name = _("Special road").." (13)",
	desc = _("Special multilane road, oneway 3-lane, speed limit %2%."),
	categories = { "05_spezial" },
	borderGroundTex = "street_border.lua",
	materials = {
		streetPaving = {
			name = "street/country_new_medium_paving.mtl",
			size = { 8.0, 8.0 }
		},		
		streetBorder = {
			name = "street/new_large_border.mtl",
			size = { 6.0, .5 }
		},
		junctionBorder = {
			name = "street/new_large_border.mtl",
			size = { 6.0, .5 }
		},			
		streetLane = {
			name = "street/new_medium_lane.mtl",
			size = { 4.0, 4.0 }
		},
		streetArrow = {
			name = "street/default_arrows.mtl",
			size = { 9.0, 3.0 }
		},
		streetStripe = {
			name = "street/new_medium_stripes.mtl",
			size = { 8.0, .5 }		
		},
		streetStripeMedian = {
			name = "street/new_large_median.mtl",
			size = { 4.0, 1 }
		},
		streetTram = {
			name = "street/new_medium_tram_paving.mtl",
			size = { 2.0, 2.0 }
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		streetBus = {
			name = "street/new_medium_bus.mtl",
			size = { 12, 2.7 }
		},
		crossingLane = {
			name = "street/new_medium_lane.mtl",
			size = { 4.0, 4.0 }
		},
		crossingBus = {
			name = "",
		},
		crossingTram = {
			name = "street/new_medium_tram_paving.mtl",
			size = { 2.0, 2.0 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		crossingCrosswalk = {
			name = "street/new_medium_crosswalk.mtl",
			size = { 3.0, 2.5 }
		},
		sidewalkPaving = {
			name = "street/new_medium_sidewalk.mtl",
			size = { 4.0, 2.0 }
		},
		sidewalkLane = {	

		},
		sidewalkBorderInner = {
			name = "street/new_medium_sidewalk_border_inner.mtl",		
			size = { 3, 0.6 }
		},
		sidewalkBorderOuter = {
			name = "street/new_medium_sidewalk_border_outer.mtl",		
			size = { 8.0, 0.41602 }
		},
		sidewalkCurb = {
			name = "street/new_medium_sidewalk_curb.mtl",
			size = { 3, .35 }
		},
		sidewalkWall = {
			name = "street/new_medium_sidewalk_wall.mtl",
			size = { 8.0, 0.41602 }
		}	
	},
	assets = {
		{
			name = "street/street_light_eu_c.mdl",
			offset = 8.0,
			distance = 16.0,
			prob = 1.0,
			offsetOrth = 3.1,
			randRot = false,
			oneSideOnly = false,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	signalAssetName = "asset/ampel.mdl",
	cost = 60.0,
}
end
