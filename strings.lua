function data()
	return {
		en = {
			["Name"] = ("RTP - Roads Pack"),
			["Description"] = ("Roads´n Trams Project (RTP):\n\nA variety of roads. Including dirt roads and pedestrian roads."),
		
			-- Categories
			["(RTP) Fusswege"] = ("(RTP) Footpath"),
			["(RTP) Feldwege"] = ("(RTP) Rural roads"),
			["(RTP) Landstrassen"] = ("(RTP) Country roads"),
			["(RTP) Stadtstrassen"] = ("(RTP) Urban streets"),
			["(RTP) Spezialstrassen"] = ("(RTP) Special streets"),
			
			-- Component Name
			["Country road asphalt small"] = ("Country road asphalt small"),
			["Urban road asphalt small"] = ("Urban road asphalt small"),
		
			-- Paint Brushes
			["RTP - roter Schotter"] = ("RTP - red Gravel"),
			["RTP - Strassenschotter"] = ("RTP - Roadgravel"), 
		},
        de = {
			["Name"] = ("RTP - Strassen Paket"),
			["Description"] = ("Roads´n Trams Project (RTP):\n\nEine vielzahl an verschiedenen Strassen-, Feld- und Fusswegen."),

			-- Categories
			["(RTP) Fusswege"] = ("(RTP) Fusswege"),
			["(RTP) Feldwege"] = ("(RTP) Feldwege"),
			["(RTP) Landstrassen"] = ("(RTP) Landstrassen"),
			["(RTP) Stadtstrassen"] = ("(RTP) Stadtstrassen"),
			["(RTP) Spezialstrassen"] = ("(RTP) Spezialstrassen"),			
			
			-- Component Name			
			["Footpath transparent"] = ("Fussweg unsichtbar"),
			["Footpath dirt soil"] = ("Fussweg Erde"),
			["Footpath ballast"] = ("Fussweg Schotter"),
			["Footpath red ballast"] = ("Fussweg roter Schotter"),
			["Footpath asphalt"] = ("Fussweg Asphalt"),
			
			["Rural road transparent"] = ("Feldweg unsichtbar"),
			["Rural road dirt soil"] = ("Feldweg Erde"),
			["Rural road ballast"] = ("Feldweg Schotter"),
			
			["Country road transparent"] = ("Landstrasse unsichtbar"),
			["Country road dirt soil"] = ("Landstrasse Erde"),
			["Country road ballast"] = ("Landstrasse Schotter"),
			["Country road asphalt"] = ("Landstrasse Asphalt"),

			["Urban road transparent"] = ("Stadtstrasse unsichtbar"),
			["Urban road dirt soil"] = ("Stadtstrasse Erde"),
			["Urban road ballast"] = ("Stadtstrasse Schotter"),
			["Urban road asphalt"] = ("Stadtstrasse Asphalt"),
			
			["Special road"] = ("Spezialstrasse"),
			
			["Country road asphalt small"] = ("Landstrasse Asphalt klein"),
			["Urban road asphalt small"] = ("Stadtstrasse Asphalt klein"),
			
			-- Component Descritption
			["Hold 'Shift' while painting with any brush texture to overpaint the ground texture."] = ("Halte 'Shift'-Taste gedrückt um mit dem Mal-Tool darüber zu malen."),

			["Oneway road, 1-lane, speed limit %2%."] = ("Einbahnstrasse 1-spurig, Tempo %2%"),
			["Oneway road, 2-lane, speed limit %2%."] = ("Einbahnstrasse 2-spurig, Tempo %2%"),			
			["Oneway road, 3-lane, speed limit %2%."] = ("Einbahnstrasse 3-spurig, Tempo %2%"),

			["Road, 2-lane, speed limit %2%."] = ("Strasse, 2-spurig, Tempo %2%"),
			["Road, 4-lane, speed limit %2%."] = ("Strasse, 4-spurig, Tempo %2%"),
			["Road, 6-lane, speed limit %2%."] = ("Strasse, 6-spurig, Tempo %2%"),
			
			["Special multilane road, oneway 2-lane, speed limit %2%."] = ("Spezialstrasse multispur, Einbahn 2-spurig, Tempo %2%"),
			["Special multilane road, oneway 3-lane, speed limit %2%."] = ("Spezialstrasse multispur, Einbahn 3-spurig, Tempo %2%"),
			["Special multilane road, 4-lane, speed limit %2%."] = ("Spezialstrasse multispur, 4-spurig, Tempo %2%"),
			["Special multilane road, 6-lane, speed limit %2%."] = ("Spezialstrasse multispur, 6-spurig, Tempo %2%"),

			-- Paint Brushes
			["RTP - roter Schotter"] = ("RTP - roter Schotter"),
			["RTP - Strassenschotter"] = ("RTP - Strassenschotter"), 
		
		},


		


	}
end